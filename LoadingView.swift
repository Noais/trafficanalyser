//
//  LoadingView.swift
//  TrafficAnalyser
//
//  Created by Noais on 05/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingView: UIView {

    var activityIndicatorView: NVActivityIndicatorView!
    
    override func draw(_ rect: CGRect) {
        self.activityIndicatorView = NVActivityIndicatorView(frame: self.frame,
                                                             type: NVActivityIndicatorType.orbit,
                                                             color: UIColor.gray)
        
        self.activityIndicatorView.padding = 20
        
        self.addSubview(self.activityIndicatorView)
        
        self.startAnimating()
    }
    
    func stopAnimating(){
        self.activityIndicatorView.stopAnimating()
    }
    
    private func startAnimating() {
        self.activityIndicatorView.startAnimating()
    }

}
