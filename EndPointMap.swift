//
//  EndPointsMap.swift
//  TrafficAnalyser
//
//  Created by Noais on 03/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import Foundation
import MapKit

class EndPointMap: NSObject, MKAnnotation {
    let title: String?
    let type: EndPointType
    let coordinate: CLLocationCoordinate2D
    
    init(endPoint: EndPoint) {
        self.title = endPoint.type?.toString()
        self.type = endPoint.type!
        self.coordinate =  CLLocationCoordinate2DMake(endPoint.coordinates!.1, endPoint.coordinates!.0)
        
        super.init()
    }
    
    
    // MARK: - MapKit related methods
    
    // pinTintColor for traffic tipe: slowTraffic, accident, stationaryTraffic, queuingTraffic, other
    func pinTintColor() -> UIColor  {
        switch type as EndPointType{
        case .accident, .stationaryTraffic:
            return MKPinAnnotationView.redPinColor()
        case .heavyTraffic, .slowTraffic, .queuingTraffic:
            return MKPinAnnotationView.purplePinColor()
        default:
            return MKPinAnnotationView.greenPinColor()
        }
    }
    
}
