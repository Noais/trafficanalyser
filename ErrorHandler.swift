//
//  ErrorHandler.swift
//  TrafficAnalyser
//
//  Created by Noais on 05/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit
import BRYXBanner

class ErrorHandler: NSObject {
    
    static let sharedInstance = ErrorHandler()
    var errorBanner: Banner?
    
    func handleLoadEndPointsError(_ error: Error) {
        print(error)
        switch error {
        case APIManagerError.network(let innerError as NSError):
            // check the domain
            if innerError.domain != NSURLErrorDomain {
                break
            }
            // check the code:
            if innerError.code == NSURLErrorNotConnectedToInternet {
                showNotConnectedBanner()
                return
            }
        default:
            break
        }
    }
    
    
    func showNotConnectedBanner() {
        // check for existing banner
        if let existingBanner = self.errorBanner {
            existingBanner.dismiss()
        }
        // show not connected error & tell em to try again when they do have a connection
        self.errorBanner = Banner(title: "No Internet Connection",
                                  subtitle: "Could not load end points list." +
            " Try again when you're connected to the internet",
                                  image: nil,
                                  backgroundColor: .red)
        self.errorBanner?.dismissesOnSwipe = true
        self.errorBanner?.show(duration: nil)
    }
    
}
