//
//  EndPointsAPIManager.swift
//  TrafficAnalyser
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import Foundation
import Alamofire

enum APIManagerError: Error {
    case network(error: Error)
    case apiProvidedError(reason: String)
    case authCouldNot(reason: String)
    case authLost(reason: String)
    case objectSerialization(reason: String)
}

class EndPointsAPIManager {
    static let sharedInstance = EndPointsAPIManager()

    func fetchEndPoints(completionHandler: @escaping (Result<[EndPoint]>) -> Void) {
        Alamofire.request(Router.listOfEndPoints)
            .responseJSON { response in
                let result = self.endPointArrayFromResponse(response: response)
                completionHandler(result)
        }
    }
    
    func fetchEndPoint(endPoint: EndPoint!, completionHandler: @escaping (Result<EndPoint>) -> Void) {
        Alamofire.request(Router.createEndPoint(endPoint))
            .responseJSON { response in
                let result = self.endPointFromResponse(endPoint: endPoint, response: response)
                completionHandler(result)
        }
    }
    
    private func endPointFromResponse(endPoint: EndPoint!, response: DataResponse<Any>) -> Result<EndPoint>{
        guard response.result.error == nil else {
            print(response.result.error!)
            return .failure(APIManagerError.network(error: response.result.error!))
        }
        
        let value = response.result.value as? NSDictionary
        
        // make sure we got JSON and it's an array
        guard let dataSource = value?["data_source"] as? [String: Any] else {
            print("didn't get dataSource object as JSON from API")
            return .failure(APIManagerError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }
        
        // get all locations
        let jsonDictionary = value?["location"] as? [String : Any]
        var trunk = jsonDictionary?["trunk_end_point"] as? [String: Any]
        if let coord = trunk?["coordinates"] as? [Double]{
            endPoint.trunkEndPoint = (coord[0],coord[1])
        }
        trunk = jsonDictionary?["trunk_start_point"] as? [String: Any]
        if let coord = trunk?["coordinates"] as? [Double]{
            endPoint.trunkStartPoint = (coord[0],coord[1])
        }
        
        
        endPoint.dataSource?.resourceName = dataSource["name"] as? String
        
        return .success(endPoint)
        
    }
    
    private func endPointArrayFromResponse(response: DataResponse<Any>) -> Result<[EndPoint]> {
        guard response.result.error == nil else {
            print(response.result.error!)
            return .failure(APIManagerError.network(error: response.result.error!))
        }
    
        let value = response.result.value as? NSDictionary
        
        // make sure we got JSON and it's an array
        guard let jsonArray = value?["Objects"] as? [[String: Any]] else {
            print("didn't get array objects as JSON from API")
            return .failure(APIManagerError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }
    
        // turn JSON in to endpoints
        var endPoints = [EndPoint]()
        for item in jsonArray {
            if let endPoint = EndPoint(json: item) {
                endPoints.append(endPoint)
            }
        }
        return .success(endPoints)
    }
}
