//
//  EndPointsRouter.swift
//  TrafficAnalyser
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    static let baseURLString = "https://api.ost.pt/traffic_events"
    //TODO get keys from keychain
    static let serverAPI = "RYhzZQbRPDdAFvxyzVbBrNTkpRTOcUveSPwBeiZP"
    static let parameters = ["key": serverAPI]
    
    case listOfEndPoints
    case createEndPoint(EndPoint)
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .listOfEndPoints:
            return .get
        case .createEndPoint:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .listOfEndPoints:
            return "/"
        case .createEndPoint(let endPointId):
            return "/\(endPointId.idValue!)/"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = URL(string: Router.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        
        return try URLEncoding.default.encode(urlRequest, with: Router.parameters)
    }
}
