//
//  TrafficAnalyserUITests.swift
//  TrafficAnalyserUITests
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import XCTest

class TrafficAnalyserUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func TestFlowApplication() {
        
        
        let app = XCUIApplication()
        app.icons["TrafficAnalyser"].tap()
        app.children(matching: .window).element(boundBy: 1).children(matching: .other).element(boundBy: 1).tap()
        
        let mapMarkerButton = app.navigationBars["TrafficAnalyser.RootView"].buttons["map marker"]
        mapMarkerButton.tap()
        app.otherElements.containing(.navigationBar, identifier:"TrafficAnalyser.RootView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        
        let tablesQuery = app.tables
        tablesQuery.staticTexts["A34 southbound between A33 and M3/A272"].tap()
        app.maps.containing(.other, identifier:"Abbots Worthy").element.swipeUp()
        app.navigationBars["TrafficAnalyser.DetailView"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        mapMarkerButton.tap()
        app.navigationBars["TrafficAnalyser.MapView"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        tablesQuery.staticTexts["A34 northbound between A44 and B4027"].tap()
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .portrait
        
    }
    
    func testDetail(){
        
        let app = XCUIApplication()
        app.tables.staticTexts["A259 eastbound between A27 and B2182"].tap()
        
        let bexhillMap = app.maps.containing(.other, identifier:"Bexhill").element
        bexhillMap.swipeUp()
    }
    
    func testOrientation(){
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["M56 westbound between J11 and J12"].tap()
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .portraitUpsideDown
        XCUIDevice.shared().orientation = .landscapeLeft
        XCUIDevice.shared().orientation = .landscapeLeft
        XCUIDevice.shared().orientation = .portrait
        app.navigationBars["TrafficAnalyser.DetailView"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .portraitUpsideDown
        XCUIDevice.shared().orientation = .landscapeLeft
        XCUIDevice.shared().orientation = .landscapeLeft
        XCUIDevice.shared().orientation = .portrait
        app.navigationBars["TrafficAnalyser.RootView"].buttons["map marker"].tap()
        XCUIDevice.shared().orientation = .landscapeRight
        XCUIDevice.shared().orientation = .landscapeRight        
    }
    
    
}
