//
//  AppDelegateTests.swift
//  TrafficAnalyser
//
//  Created by Noais on 05/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import XCTest
@testable import TrafficAnalyser

class AppDelegateTests: XCTestCase {
    
    var sut: AppDelegate!
    
    
    // MARK: - Set up and tear down.
    
    override func setUp() {
        super.setUp()
        createSut()
    }
    
    func createSut() {
        sut = AppDelegate()
    }
    
    
    override func tearDown() {
        releaseSut()
        
        super.tearDown()
    }
    
    func releaseSut() {
        sut = nil
    }
    
    
    // MARK: - Basic test.
    
    func testSutIsntNil() {
        XCTAssertNotNil(sut, "Sut must not be nil.")
    }
    
    
    func testApplicationDidFinishLaunchingWithOptionsReturnsTrue() {
        XCTAssertTrue(sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil))
    }
    
    
    
    // MARK: - Stubs & Mocks.


}
