//
//  EndPointsAPIManagerTests.swift
//  TrafficAnalyser
//
//  Created by Noais on 05/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import XCTest
import Alamofire
@testable import TrafficAnalyser

class EndPointsAPIManagerTests: XCTestCase {
    
    // MARK: - Parameters & Constants.
    
    let oneDataSet = EndPoint.init(aDelayTime: 900, aIdValue: 183269, aLanguage: "en", aQueueLength: "20", aRecordTime: NSDate(), aResourceUri: "www.url.com", aType: .slowTraffic, aCoordinates: (20.0,123.0))
    var dataProvider: EndPointsAPIManager!
    
    // MARK: - Test vatiables.

    var model: EndPoint!
    
    // MARK: - Set up and tear down.
    
    override func setUp() {
        super.setUp()
      
        dataProvider = EndPointsAPIManager.sharedInstance
    }
    
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    
    // MARK: - Interaction with the model
    
    func testFetchesData() {
        dataProvider.fetchEndPoints(completionHandler: {
            (results) in
            XCTAssertTrue(results.isFailure)
        })
    }
    
    
    func testFetchedDataDetail() {
        dataProvider.fetchEndPoint(endPoint: oneDataSet, completionHandler:{
            (results) in
            XCTAssertTrue(results.isFailure)
        })
    }
}
