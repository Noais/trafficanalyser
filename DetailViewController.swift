//
//  DetailViewController.swift
//  TrafficAnalyser
//
//  Created by Noais on 03/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class DetailViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var localLabel: UILabel?
    @IBOutlet var dateTimeLabel: UILabel?
    @IBOutlet var typeLabel: UILabel?
    @IBOutlet var sourceLabel: UILabel?
    @IBOutlet var delayTimeLabel: UILabel?
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var mainView: UIView!
    
    var endPoint: EndPoint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.isHidden = true
        let completionHandler: (Result<EndPoint>) -> Void = {
            (result) in
            
            guard result.error == nil else {
                ErrorHandler.sharedInstance.handleLoadEndPointsError(result.error!)
                return
            }
            
            if self.loadingView.isHidden == false {
                self.loadingView.stopAnimating()
                self.mainView.isHidden = false
            }
            
            self.endPoint = result.value! as EndPoint
            self.sourceLabel?.text = self.endPoint.dataSource?.resourceName
            self.initMap()
        }

        EndPointsAPIManager.sharedInstance.fetchEndPoint(endPoint: endPoint, completionHandler: completionHandler)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YYYY hh:mm a"

        
        self.dateTimeLabel?.text = dayTimePeriodFormatter.string(from: self.endPoint.recordTime as! Date)
        self.localLabel?.text = self.endPoint.endPointDescription
        self.typeLabel?.text = self.endPoint.type?.toString()
        self.delayTimeLabel?.text = String((self.endPoint.delayTime! / 60)) + " minutes"
        self.background.addBlurEffect()
        map.delegate = self
    }
    
    private func initMap(){
        let mapPoint = EndPointMap.init(endPoint: endPoint)
        // set initial location
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(mapPoint.coordinate, 40000, 40000)
        map.setRegion(coordinateRegion, animated: true)
        map.addAnnotation(mapPoint)
        
        drawRouteMap()
    }
    
    private func drawRouteMap(){
        let sourcePlacemark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(endPoint.trunkEndPoint!.1, endPoint.trunkEndPoint!.0), addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(endPoint.trunkStartPoint!.1, endPoint.trunkStartPoint!.0), addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        map.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.map.add((route.polyline), level: MKOverlayLevel.aboveRoads)
        }

    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = endPoint.type?.color()
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    
}
