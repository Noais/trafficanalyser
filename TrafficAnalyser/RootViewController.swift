//
//  ViewController.swift
//  TrafficAnalyser
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit
import Alamofire



class RootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var mapButton: UIBarButtonItem!
    @IBOutlet var table: UITableView?
    var endPoints = [EndPoint]()
    let cellIdentifier = "cell"
    var refreshControl: UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapButton.isEnabled = false
        
        
        self.background.addBlurEffect()
        
        
        self.refreshControl = UIRefreshControl()
        let attributedStringColour : NSDictionary = [NSForegroundColorAttributeName : UIColor.white];
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Refreshing", attributes: attributedStringColour as? [String : AnyObject])
        self.refreshControl?.tintColor = UIColor.white
        self.refreshControl?.addTarget(self, action: #selector(RootViewController.loadEndPoints(_:)), for: UIControlEvents.valueChanged)
        self.table?.addSubview(refreshControl)
        
        loadEndPoints(self.refreshControl)
    }
    
    
    func loadEndPoints(_ refreshControl: UIRefreshControl) {
        
        let completionHandler: (Result<[EndPoint]>) -> Void = {
            (result) in
            
            if self.loadingView.isHidden == false {
                self.loadingView.stopAnimating()
                self.loadingView.isHidden = true
                self.mapButton.isEnabled = true
                self.table?.isHidden = false
            }
            
            refreshControl.endRefreshing()
            
            // tell refresh control it can stop showing up now
            if self.refreshControl != nil,
                self.refreshControl!.isRefreshing {
                self.refreshControl?.endRefreshing()
            }
            
            guard result.error == nil else {
                ErrorHandler.sharedInstance.handleLoadEndPointsError(result.error!)
                return
            }
            
            self.endPoints = result.value! as Array<EndPoint>
            self.table?.reloadData()
        }
        
        EndPointsAPIManager.sharedInstance.fetchEndPoints(completionHandler: completionHandler)
    }
    
    //MARK:  UITableViewDelegate Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.endPoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EndPointCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? EndPointCell
        
        if (cell != nil)
        {
            let row = indexPath.row
            cell.localNameLabel?.text = self.endPoints[row].endPointDescription
            cell.dateLabel?.text = Utils.convertTime(date: self.endPoints[row].recordTime as! Date)
            cell.eventTypeLabel?.text = self.endPoints[row].type?.toString()
            cell.createCircle(type: self.endPoints[row].type!)
        }
        return cell
    }
    
    // MARK:  UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        secondViewController.endPoint = self.endPoints[indexPath.row]
        
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap"
        {
            if let mapViewController = segue.destination as? MapViewController {
                mapViewController.endPointMap = endPoints 
            }
        }
    }
}



