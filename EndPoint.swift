//
//  EndPoint.swift
//  TrafficAnalyser
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit

enum EndPointType: String {
    case slowTraffic = "slowTraffic"
    case accident = "accident"
    case stationaryTraffic = "stationaryTraffic"
    case queuingTraffic = "queuingTraffic"
    case unspecifiedAbnormalTraffic = "unspecifiedAbnormalTraffic"
    case heavyTraffic = "heavyTraffic"
    case other = "other"
    
    
    func toString() -> String{
        switch self {
        case .slowTraffic:
            return "Slow Traffic"
        case .accident:
            return "ACCIDENT"
        case .stationaryTraffic:
            return "Stationary Traffic"
        case .queuingTraffic:
            return "Queuing Traffic"
        case .unspecifiedAbnormalTraffic:
            return "Abnormal Traffic"
        case .heavyTraffic:
            return "Heavy Traffic"
        case .other:
            return "Other"
        }
    }
    
    func color() -> UIColor{
        switch self {
        case .accident, .stationaryTraffic:
            return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        case .heavyTraffic, .slowTraffic, .queuingTraffic:
            return #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
            
        default:
            return #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
        
    }
}

class EndPoint: NSObject, NSCoding {

    var delayTime: Int?
    var idValue: Int!
    var language: String?
    var queueLength: String?
    var recordTime: NSDate?
    var dataSource: DataSource?
    var endPointDescription: String?
    var type: EndPointType?
    var coordinates: (Double, Double)?
    var trunkEndPoint: (Double, Double)?
    var trunkStartPoint: (Double, Double)?

    required init?(aDelayTime: Int?, aIdValue: Int!, aLanguage: String?, aQueueLength: String?, aRecordTime: NSDate?, aResourceUri: String?, aType: EndPointType?, aCoordinates: (Double, Double)?) {
        self.delayTime = aDelayTime
        self.idValue = aIdValue
        self.language = aLanguage
        self.queueLength = aQueueLength
        self.recordTime = aRecordTime
        self.dataSource = DataSource(resourceUri: aResourceUri)
        self.type = aType
        self.coordinates = aCoordinates
    }
    
    required override init() {
    }
    
    required init?(json: [String: Any]) {
        
        self.delayTime = json["delay_time_value"] as? Int
        self.idValue = json["id"] as? Int
        
        if let location = json["location"] as? [String: Any] {
            if let coord = location["coordinates"] as? [Double]{
                self.coordinates = (coord[0],coord[1])
            }
            if let names = location["names"] as?  [[String: Any]] {
                if let descriptor = names[0]["descriptor"] as? [String: Any] {
                    self.language = descriptor["lang"] as? String
                    self.endPointDescription = descriptor["value"] as? String
                }
            }
        }
        
        self.queueLength = json["queue_length"] as? String
        self.recordTime = NSDate(timeIntervalSince1970: json["record_time"] as! TimeInterval)
        self.dataSource = DataSource.init(resourceUri: json["resource_uri"] as! String!)
        
        let typeStr = json["type"] as? String
        self.type = EndPointType(rawValue: typeStr!)
    }
    
    // MARK: NSCoding
    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode(self.delayTime, forKey: "delayTime")
        aCoder.encode(self.idValue, forKey: "idValue")
        aCoder.encode(self.language, forKey: "language")
        aCoder.encode(self.queueLength, forKey: "queueLength")
        aCoder.encode(self.recordTime, forKey: "recordTime")
        aCoder.encode(self.dataSource?.resourceUri, forKey: "resourceUri")
        aCoder.encode(self.endPointDescription, forKey: "endPointDescription")
        aCoder.encode(self.type, forKey: "type")
        aCoder.encode(self.coordinates, forKey: "coordinates")
        aCoder.encode(self.trunkEndPoint, forKey: "trunkEndPoint")
        aCoder.encode(self.trunkStartPoint, forKey: "trunkStartPoint")
    }
    
    @objc required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.delayTime = aDecoder.decodeObject(forKey: "delayTime") as? Int
        self.idValue = aDecoder.decodeObject(forKey: "idValue") as? Int
        self.language = aDecoder.decodeObject(forKey: "language") as? String
        self.queueLength = aDecoder.decodeObject(forKey: "queueLength") as? String
        self.recordTime = aDecoder.decodeObject(forKey: "recordTime") as? NSDate
        self.dataSource?.resourceUri = aDecoder.decodeObject(forKey: "resourceUri") as? String
        self.endPointDescription = aDecoder.decodeObject(forKey: "endPointDescription") as? String
        self.type = aDecoder.decodeObject(forKey: "type") as? EndPointType
        self.coordinates = aDecoder.decodeObject(forKey: "coordinates") as? (Double, Double)
        self.trunkEndPoint = aDecoder.decodeObject(forKey: "trunkEndPoint") as? (Double, Double)
        self.trunkStartPoint = aDecoder.decodeObject(forKey: "trunkStartPoint") as? (Double, Double)
    }

}


