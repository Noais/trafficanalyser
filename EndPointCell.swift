//
//  EndPointCell.swift
//  TrafficAnalyser
//
//  Created by Noais on 02/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit


class EndPointCell: UITableViewCell {
    
    @IBOutlet weak var localNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var typeIndicator: UIView!
    
    
    func createCircle(type: EndPointType){
    
        let point = CGPoint(x: typeIndicator.frame.size.width/2,y: typeIndicator.frame.size.height/2)
        let circlePath = UIBezierPath(arcCenter: point, radius: CGFloat(5), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = type.color().cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = type.color().cgColor
        //you can change the line width
        shapeLayer.lineWidth = 3.0
        
        typeIndicator.layer.addSublayer(shapeLayer)
    }
}
