//
//  Utils.swift
//  TrafficAnalyser
//
//  Created by Noais on 03/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    static func convertToEndPointsMap(endPoints: [EndPoint]) -> [EndPointMap]{
        var mapPointsArray = [EndPointMap]()
        
        for endPoint in endPoints {
            let mapPoint = EndPointMap(endPoint: endPoint)
            mapPointsArray.append(mapPoint)
        }
        
        return mapPointsArray
    }
    
    static func convertTime(date: Date) -> String{
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YYYY hh:mm a"
    
        return dayTimePeriodFormatter.string(from: date)
    }
}
