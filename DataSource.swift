//
//  DataSource.swift
//  TrafficAnalyser
//
//  Created by Noais on 05/12/2016.
//  Copyright © 2016 David Ferreira. All rights reserved.
//

import UIKit

class DataSource: NSObject {
    
    var resourceUri: String?
    var resourceName: String?
    
    
    required init(resourceUri :String!) {
        self.resourceUri = resourceUri
    }
    
    
}
